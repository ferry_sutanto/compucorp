<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Compucorp task</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

  <!-- Styles -->
  <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">

</head>
<body ng-controller="HomeCtrl">
  <div class="container">
    <div class="header clearfix">
      <h3 class="text-muted"> Weather Detect </h3>
    </div>
    <div class="row">
      <div class="col-xs-12 col-md-6 col-md-offset-3" ng-if="weather==null">
        <p ng-cloak ng-show="search.shareLocation==null">
          Please share your location to enable us to detect the weather at your place
        </p>
        <form class="form" ng-show="search.shareLocation==false" ng-cloak>
          <div class="form-group">
            <label>Select country</label>
            <country-select ng-model="search.country" class="form-control" required></country-select>
          </div>
          <div class="form-group">
            <label>Your zip code</label>
            <input type="text" ng-model="search.zip" class="form-control" required/>
          </div>
          <div class="form-group">
            <button type="button" class="btn btn-primary" ng-click="searchLocation()">Find weather</button>
          </div>
        </form>
      </div>

      <div class="col-xs-12 col-md-6 col-md-offset-3" ng-if="weather!=null">
        <table class="table">
          <tr>
            <td> <img ng-src="http://openweathermap.org/img/w/@{{ weather.icon }}.png" /> </td>
            <td style="vertical-align:middle;"> Weather : @{{ weather.name }} </td>
          </tr>
        </table>
      </div>
    </div>

    <footer class="footer">
      <p>&copy; 2016 Ferry Sutanto.</p>
      <script src="{{asset('js/test.js')}}"></script>
    </footer>
  </div>
</body>
</html>
