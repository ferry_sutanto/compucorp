require('angular');
require('ngMap');
require('ng-country-select/dist/ng-country-select.js');
require('ngGeolocation/ngGeolocation.js');
require('angular-openweathermap-api-factory');

'use strict';

var myApp = angular.module('myApp', [
  'ngMap',
  'ngGeolocation',
  'jtt_openweathermap',
  'countrySelect',
]).controller('HomeCtrl', [
  '$scope',
  '$geolocation',
  'NgMap',
  'openweathermapFactory',
  function($scope, $geolocation, NgMap, openweathermapFactory) {
    $scope.search = {
      shareLocation : null,
      query: '',
      zip: null,
      country: null
    };

    $scope.weather = null;

    //initialize get user location
    $geolocation.getCurrentPosition({
      timeout: 6000
    }).then(function(position) {
      //permission accepted
      $scope.search.shareLocation = true;
      openweathermapFactory.getWeatherFromLocationByCoordinates({
        lat: position.coords.latitude,
        lon: position.coords.longitude,
        appid: "808ea5200ab2a832d9f2fc3880b72964",
      }).then(function(response){
        $scope.weather = {
          name : response.data.weather[0].description,
          icon : response.data.weather[0].icon
        };
      }).catch(function (err) {
        console.log(err);
      });
    }, function(err){
      if (err.error.code==1) {
        //permission denied
        $scope.search.shareLocation = false;
      } else if (err.error.code==1) {
        //location unavailable
      } else if (err.error.code==1) {
        //timeout
      } else {
        //other
      }
    });

    $scope.searchLocation = function() {
      openweathermapFactory.getWeatherFromLocationByZipcode({
        zip: $scope.search.zip+","+$scope.search.country,
        lang: "en",
        appid: "808ea5200ab2a832d9f2fc3880b72964",
      }).then(function(response){
        $scope.weather = {
          name : response.data.weather[0].description,
          icon : response.data.weather[0].icon
        };
      }).catch(function (err) {
        console.log(_data);
      });
    };


  }
]);
